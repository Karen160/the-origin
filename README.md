# The Origin

Lors de mon stage en 2ème année de développement web, j'ai créé le site The Origin en pour l'agence Purée Maison.

Lecomte Asia est une société qui vend principalement des produits vietnamiens de haut de gamme comme de la bière ou du café. Cherchant continuellement à élargir sa gamme de produit vietnamien, elle distribue la gamme de café appelé The Origin.

Pour promouvoir cette nouvelle gamme, Lecomte Asia souhaitait créer un site internet, elle a donc missionné l'agence Purée Maison pour la création de leur site internet. J’ai donc été choisi pour créer entièrement ce site en utilisant Wordpress et ses plugins.
 
Pour cette réalisation, le client m’avait fourni un wording contenant les pages à réaliser avec leurs contenus, leurs logos et la photo de trois paquets de café.
J’ai donc créé huit pages : une page d’accueil, une page de présentation de l’entreprise, une autre pour présenter les trois cafés, une page listant chaque café avec un lien sur une page d’achat et pour finir une page contact.

C’est avec le thème Wordpress Corretto, un thème spécialisé dans le café, que j’ai pu créer la trame du site. Ce thème m’a permis de personnaliser l’apparence globale de mon site en disposant à ma guise son contenu et son visuel. 

J’ai utilisé deux plugins de Wordpress :
- WPBackery Page Builder, qui m'a permis de m'être en place le style que je souhaitais appliquer au site ainsi que le contenu disponible dans le wording du client
- WooCommerce, où j’ai pu gérer tous les éléments concernant la vente de ses produits via une page d’achat par produit.

Pour ce site, j'ai dû créer et modifier des illustrations avec le logiciel Illustrator afin de répondre aux attentes du client. J'ai terminé le site en effectuant les derniers retours du client lorsque j'étais en freelance.

Consultez le site via ce lien : https://originvietnam.com.vn/
